#include <string>
#include <fstream>
#include <iostream>
#include<vector>
#include <thread>
#include <Windows.h>
#include <ctime>

void printVector(std::vector<int> primes);

void removeNonPrimes(int begin, int end, std::vector<int>& primes);

void writePrimesToFile(int begin, int end, std::ofstream& file);
void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N);
