#pragma once
#include <iostream>
#include <set>
#include <queue>
#include <string>
#include <Windows.h>
#include <fstream>
#include <mutex>
#include <condition_variable>
#define TEN_SEC 10000
#undef max//win.h defines max, I use a word max in the cin.ignore so I undef it.
class MessagesSender
{
	std::set<std::string> activeUsers;
public:
	void printMenu();
	void signIn();
	void signOut();
	void connectedUsers();
	void manage();//This function uses the other functions to create a working list of options.
	std::set<std::string> getUsers() { return activeUsers; };
	friend void spreadMsg(std::queue<std::string>& lineQueue, const MessagesSender& m);
	MessagesSender();
	~MessagesSender();
};

void fileToQueue(std::queue<std::string>& lineQueue);//reads from data.txt
void spreadMsg(std::queue<std::string>& lineQueue, const MessagesSender& m);//writes to the output file
