#include "threads.h"
#define DELETED -1
#include <mutex>

std::mutex mtx;
//this function leaves the -1s in the vector. (so any number from begin to end that isn't a prime will be -1)
void removeNonPrimes(int begin, int end, std::vector<int>& primes)
{
	int i = 0;
	for (int i = 2; i < end; i++) primes.push_back(i);

	if (1 >= begin)
		begin = 2;
	while (true)
	{
		if (primes[i] * primes[i] > end)
		{
			break;
		}
		if (DELETED == primes[i] )
		{
			i++;
			continue;
		}
		for (int k = primes[i] * 2; k < end; k += primes[i])
		{
			primes[k - 2] = DELETED;
		}
		i++;
		while (primes[i] == DELETED)
		{
			i++;
		}
	}
}

//prints the vector
void printVector(std::vector<int> primes)
{
	int len = primes.size();
	for (int i = 0; i < len; i++)
	{
		std::cout << primes[i] << std::endl;
	}
}

//this function writes all prime numbers from begin to end to the passed file.
void writePrimesToFile(int begin, int end, std::ofstream & file)
{
	std::vector<int> primes;
	int len = 0;
	int i = begin;
	removeNonPrimes(begin, end, std::ref(primes));
	len = primes.size();
	while (i < (end > len ? len:end))
	{
		if (primes[i] != DELETED)
		{
			try
			{
				mtx.lock();
				file << primes[i] << std::endl;
				mtx.unlock();
			}
			catch (...)//if we can't write to the file for some reason, we must make sure the lock will be unlocked.
			{
				mtx.unlock();
				std::cout << "Failed to write to file" << std::endl;
			}
		}
		i++;
	}
}

//this function writes all prime numbers from begin to end to the passed filePath, by creating N threads each writing an equal amount of prime numbers.
void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N)
{

	int step = end / N;
	std::ofstream file;
	std::vector<std::thread *> allThreads;
	clock_t time = clock();
	std::thread *t = 0;
	file.open(filePath, std::ios::out);
	for (int i = 0; i < N; i++)
	{
		t = new std::thread(writePrimesToFile, i * step, i * step + step, std::ref(file));
		allThreads.push_back(t);
	}
	for (int i = 0; i < (int)allThreads.size(); i++)
	{
		allThreads[i]->join();
		delete allThreads[i];
	}
	std::cout << "time passed: " << clock() - time << " milliseconds" << std::endl;
}
