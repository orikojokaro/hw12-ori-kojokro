#include "MessagesSender.h"
#include <thread>
int main()
{
	MessagesSender m;
	std::queue<std::string> qu;
	std::thread th1(fileToQueue, std::ref(qu));
	th1.detach();
	std::thread th2(spreadMsg, std::ref(qu), std::ref(m));
	th2.detach();
	m.manage();
	return 0;
}