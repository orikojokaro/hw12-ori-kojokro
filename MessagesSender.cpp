#include "MessagesSender.h"

std::mutex mtxCout;//for cout(printing)
std::mutex mtxUsers;//active users
std::mutex mtxLines;//lines from file. (the queue)
std::mutex mtxCon;//mutex for condition variable
std::mutex mtxConUser;//mutex for waiting for users to sign in
std::condition_variable cv;
std::condition_variable cvUser;//condition variable for users to sign in
void MessagesSender::printMenu()
{
	using namespace std;
	std::lock_guard<std::mutex> mtx(mtxCout);
	cout << "1.Sign in" << endl << "2.Signout" << endl
		<< "3.Connected Users" << endl << "4.exit" << endl;
}
//makes a user active. If the user is already active, it will not re-add him.
void MessagesSender::signIn()
{
	std::string name = "";
	std::unique_lock<std::mutex> lck(mtxCout);
	std::cout << "Enter your username: ";
	lck.unlock();
	std::cin >> name;
	std::unique_lock<std::mutex> lckUsers(mtxUsers);
	if (activeUsers.find(name) == activeUsers.end())
	{
		activeUsers.insert(name);
		lckUsers.unlock();
		lck.lock();
		std::cout << "Added you." << std::endl;
		lck.unlock();
		cvUser.notify_all();
	} 
	else
	{
		lckUsers.unlock();
		lck.lock();
		std::cout << "Already active" << std::endl;
		lck.unlock();
	}
}
//this function removes a person from the active users list.
void MessagesSender::signOut()
{
	std::string name = "";
	std::unique_lock<std::mutex> lck(mtxCout);
	std::cout << "Enter your username: ";
	lck.unlock();
	std::cin >> name;
	std::unique_lock<std::mutex> lckUsers(mtxUsers);
	if (activeUsers.find(name) == activeUsers.end())
	{
		do {
			lckUsers.unlock();
			lck.lock();
			std::cout << "Not active. Try again: ";
			lck.unlock();
			std::cin >> name;
		} while (activeUsers.find(name) == activeUsers.end());
	}
	else
	{
		lckUsers.unlock();
	}
	lckUsers.lock();//the code is more readable with this parts outside the if.... else. (at the price of slightly longer runtime)
	activeUsers.erase(name);
	lckUsers.unlock();
}
//prints the connected users.
void MessagesSender::connectedUsers()
{
	std::unique_lock<std::mutex> lck(mtxCout);
	lck.unlock();
	std::set<std::string>::iterator iter = activeUsers.begin();
	std::unique_lock<std::mutex> lckUsers(mtxUsers);
	while (iter != activeUsers.end())
	{
		lck.lock();
		std::cout << *iter << std::endl;
		lck.unlock();
		iter++;
	}
}
//this function uses all of the function of the class MessagesSender to create a working interface for adding users to the active users list.
void MessagesSender::manage()
{
	enum {
		INPUT = 1,
		OUTPUT,
		ALL,
		EXIT
	};
	std::unique_lock<std::mutex> lck(mtxCout);
	lck.unlock();
	short int input = 0;
	while (input != EXIT)
	{
		printMenu();
		lck.lock();
		std::cout << "Select an option: " << std::endl;
		lck.unlock();
		std::cin >> input;
		while (std::cin.fail() || input > EXIT || input < INPUT)
		{
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			lck.lock();
			std::cout << "Input too big or small. Try again: " << std::endl;
			lck.unlock();
			std::cin >> input;
		}
		switch (input)
		{
		case INPUT:
			signIn();
			break;
		case OUTPUT:
			signOut();
			break;
		case ALL:
			connectedUsers();
			break;
		case EXIT:
			break;
		}
	}
}
//this function takes the data from the data.txt file and inserts it line by line into a queue.
void fileToQueue(std::queue<std::string>& lineQueue)
{
	std::fstream dataFile;
	std::ofstream dataFileTrunc;
	std::string line;
	std::unique_lock<std::mutex> lck(mtxCout);
	lck.unlock();
	std::unique_lock<std::mutex> lckLines(mtxLines);
	lckLines.unlock();
	int sleepFor = 60 *1000;//60 seconds, *1000 becuase its in milliseconds.
	while (true)
	{
		dataFile.open("data.txt", std::ios::in);
		if (dataFile.is_open())
		{
			while (getline(dataFile, line))
			{
				lckLines.lock();
				lineQueue.push(line);
				lckLines.unlock();
			}
			dataFile.close();
			dataFileTrunc.open("data.txt", std::ios::trunc);
			if (dataFileTrunc.is_open())
			{
				dataFileTrunc << "";
				dataFileTrunc.close();
			}
			else
			{
				lck.lock();
				std::cout << "couldn't open the file " << std::endl;
				lck.unlock();
			}
		}
		else
		{
			lck.lock();
			std::cout << "couldn't open the file" << std::endl;
			lck.unlock();
		}
		cv.notify_all();
		Sleep(sleepFor);//For myself: prehaps use lock instead?
	}
}
//this function writes the queued data of the massage sender (which is inside of the MessagesSender class) to an output file given to it as a parameter.
void spreadMsg(std::queue<std::string>& lineQueue, const MessagesSender& m)
{
	std::ofstream dataFile;
	std::set<std::string>::iterator iter = m.activeUsers.begin();
	std::unique_lock<std::mutex> lck(mtxCout);
	lck.unlock();
	std::unique_lock<std::mutex> lckUsers(mtxUsers);
	lckUsers.unlock();
	std::unique_lock<std::mutex> lckLines(mtxLines);
	lckLines.unlock();
	std::unique_lock<std::mutex> lckCon(mtxCon);
	lckCon.unlock();
	std::unique_lock<std::mutex> lckConUser(mtxConUser);
	lckConUser.unlock();
	dataFile.open("output.txt", std::ios::out);
	while (true)
	{
		if (dataFile.is_open())
		{
			lckUsers.lock();
			iter = m.activeUsers.begin();
			if (iter != m.activeUsers.end())//are there  users
			{
				lckUsers.unlock();
				lckLines.lock();
				while (0 < lineQueue.size())
				{
					lckUsers.lock();
					iter = m.activeUsers.begin();
					while (iter != m.activeUsers.end())
					{
						dataFile << *iter << ": " << lineQueue.front() << std::endl;
						iter++;
					}
					lckUsers.unlock();
					lineQueue.pop();
				}
				lckLines.unlock();
			}
			else
			{
				lckUsers.unlock();
			}
		}
		else
		{
			lck.lock();
			std::cout << "couldn't open the file" << std::endl;
			lck.unlock();
		}
		lckLines.lock();
		lckUsers.lock();
		iter = m.activeUsers.begin();
		if (0 == lineQueue.size())//only wait if the line queue isn't empty.
		{
			lckUsers.unlock();
			lckLines.unlock();
			lckCon.lock();
			cv.wait(lckCon);
			lckCon.unlock();
		}
		else if (iter == m.activeUsers.end())
		{
			lckUsers.unlock();
			lckLines.unlock();
			Sleep(TEN_SEC);//give the user 10 seconds to sign in
			iter = m.activeUsers.begin();
			if (iter == m.activeUsers.end())//if there are still no users. wait untill one is added.
			{
				lckConUser.lock();
				cvUser.wait(lckConUser);
				lckConUser.unlock();
			}
		}
	}
}

MessagesSender::MessagesSender()
{
}


MessagesSender::~MessagesSender()
{
}
