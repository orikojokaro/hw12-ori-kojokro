#include "threads.h"

#define EASY 1000
#define MEDIUM 100000
#define HARD 1000000
int main()
{
	callWritePrimesMultipleThreads(1, EASY, "primes1.txt", 5);
	callWritePrimesMultipleThreads(1, MEDIUM, "primes2.txt", 5);
	callWritePrimesMultipleThreads(1, HARD, "primes3.txt", 5);

	system("pause");
	return 0;
}